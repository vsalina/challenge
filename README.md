# Reign Challenge 


## Description
The server, once an hour, at min 0 with 0 secs, connect to the API (refer to the below url) which shows
recently posted articles about Node.js on Hacker News. It insert the data from the
API into a mongodb database and expose those records in a REST API which the client (e.g. Postman) could be used
to retrieve the data. Implemented using the [Nest framework](https://github.com/nestjs/nest).

## Installation

```bash
$ npm install
```

## Running the app
### Before start
Create your .env file and then fill the variables MONGO_USERNAME, MONGO_PASSWORD and JWT_SECRET
```
$ cp .production.env .env 
```

start mongo db 
```bash
$ docker-compose -f docker-compose.yml up server-mongodb  --build
```
and run seeder to poblate db
```bash
$ npm run seed:refresh
```

then you can start running the app
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Going to Production  
```bash
$ docker-compose -f docker-compose.yml up --build
```

## Documentation
http://localhost:3000/api/docs/

## curl testing examples

Login to get AuthToken
```curl
$ curl --location --request POST 'http://localhost:3000/api/v1/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "jose",
    "password": "123456"
}'
```

Articles find
```curl
$ curl --location --request GET 'http://127.0.0.1:3000/api/v1/articles?search=node&page=1' \
--header 'Authorization: Bearer ${AuthToken}'
```



## Support

Write an email to José Valentin Salina at josevalentinsp@gmail.com

## Stay in touch

- Author - [José Salina](https://kamilmysliwiec.com)

## License

Nest is [MIT licensed](LICENSE).
