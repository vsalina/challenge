import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Article, ArticleDocument } from './article.entity';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel(Article.name) private readonly articleModel: Model<ArticleDocument>
  ){
  }

  async insertMany(documents: Article[]) {
    return this.articleModel.insertMany(documents);
  }
  
  async find(filter, options){
    return this.articleModel.find(filter,null,options).exec();
  }

  async count(filter) {
    return this.articleModel.count(filter).exec();
  }

  async delete(id){
    return this.articleModel.deleteOne({_id: id}).exec();
  }
}
