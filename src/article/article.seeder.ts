import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {DataFactory, Seeder} from 'nestjs-seeder'
import { Article, ArticleDocument } from './article.entity';

export class ArticleSeeder implements Seeder {
  constructor(
    @InjectModel(Article.name) private readonly articleModel : Model<ArticleDocument>
  ){
  }

  drop(): Promise<any>{
    return this.articleModel.deleteMany({}) as any
  }

  seed(): Promise<any> {
    const articles = DataFactory.createForClass(Article).generate(50)
    return this.articleModel.insertMany(articles)
  }

}