import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { rejects } from 'assert';
import { Article, ArticleSchema } from './article.entity';
import { ArticleService } from './article.service';

describe('ArticleService', () => {
  let service: ArticleService;

  beforeEach(async () => {
    function mockArticleModel(dto?: any[]) {
      this.data = dto || [        {
        "_id": "614e2bc080f9203ab76d0793",
        "_tags": [
            "api"
        ],
        "parent_id": 44965684,
        "story_url": "http://hellen.info",
        "story_title": "ut illum ipsa sed rerum nihil enim",
        "story_id": 34417,
        "num_comments": 22,
        "comment_text": "in quia rerum esse molestiae quas voluptatem itaque et enim ullam consequuntur doloremque aliquam eaque aliquam dolorem facere qui eveniet dicta quod voluptates iste in",
        "author": "Name_Rogahn61",
        "url": "https://don.biz",
        "title": "praesentium rem aliquid ipsam corporis eum error veritatis odio quasi",
        "created_at": "2021-09-06T17:46:47.106Z",
        "__v": 0
    },];
      this.find  = () => {
        return this.data;
      };
      this.insertMany = (newData) => {
        return new Promise((resolve, reject) => { resolve([...this.data, ...newData])})
      }
      this.deleteOne = (id) => {
        const exec = () => {
          return new Promise(async (resolve, reject) => { 
            let before = this.data.length;
            let after = (await this.data.filter(i => i._id != id._id)).length;
            if(before > after){
              resolve ({deletedCount:1});
            }
            resolve({deletedCount:0});
          })
        }
        return {exec}
      }
    }

    const module: TestingModule = await Test.createTestingModule({
      providers: [ArticleService,
      {
        provide: getModelToken('Article'),
        useValue: new mockArticleModel(),
      },
    ]}).compile();

    service = module.get<ArticleService>(ArticleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Insert Many', async ()=>{
    const data =   [{
      "_tags": [
          "rest"
      ],
      "parent_id": 34212175,
      "story_url": "https://carol.biz",
      "story_title": "vero dolor odio eaque nisi voluptatum quia",
      "story_id": 37502759,
      "num_comments": 28,
      "comment_text": "illo enim quo veritatis consequatur sunt odit omnis aperiam delectus dicta tempore nihil et iste similique praesentium fugiat quos deleniti dignissimos ullam consectetur et et",
      "author": "Mallory.Ratke57",
      "url": "https://jordy.com",
      "title": "veniam sit sed corporis atque in aut omnis quis et",
      "created_at": new Date("2021-09-05T23:57:34.304Z"),
      "__v": 0
  }]
    expect((await service.insertMany(data)).length).toEqual(2)
  })

  it('Delete', async ()=>{
    const id = "614e2bc080f9203ab76d0793"
    expect((await service.delete(id)).deletedCount).toEqual(1)
  })
});
