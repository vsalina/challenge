import { Controller, Get, Delete, Req, Res, Param, Query,HttpStatus, Post, Logger, UseGuards } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { Response } from 'express';
import { QueryOptions } from 'mongoose';
import { ArticleService } from './article.service';
import * as moment from 'moment';
import { AlgoliaService } from './algolia/algolia.service';
import { Cron  } from '@nestjs/schedule';
import { JwtAuthGuard } from './../auth/auth.guard';

@Controller('api/v1/articles')
export class ArticleController {
  private readonly logger = new Logger(ArticleController.name);

  constructor(
    private readonly articleService: ArticleService, 
    private readonly algoliaService: AlgoliaService, 
    ) {
  }

  @UseGuards(JwtAuthGuard)
	@ApiQuery({name: "search",description: "Search value of articles to return",required: false,type: String})
  @ApiQuery({name: "page",description: "Page number to return",required: false,type: Number})
	@ApiQuery({name: "sort",description: "Sort value to return in created_at attribute",required: false,type: Number})
	@ApiQuery({name: "limit",description: "The maximum number of articles to return",required: false,type: Number})
  @Get()
  async getAll(
    @Query('search') search: string, 
    @Query('page') page: number = 1, 
    @Query('sort') sort: number = 1, 
    @Query('limit') limit: number = 5, 
    ){
    this.logger.log(`Find articles with search ${search}, page ${page}, limit: ${limit} and sort ${sort}`);

    let options: QueryOptions = {};
    let filter = {};
    if(search) {
      const month = this.parseMonth(search.toString());
      filter = {
        $or: [
          { title: new RegExp(search.toString(), 'i')},
          { author: new RegExp(search.toString(), 'i')},
          { _tags: search.toString() }, 
        ]
      }
      if(month) filter['$or'].push({ "$expr": { "$eq": [{ "$month": "$created_at" }, month] } })
    }
    if (sort) {
      options.sort= {
        created_at: sort
      }
    }

    limit = parseInt(limit as any)
    if(limit === undefined || limit > 5 || limit < 1) limit = 5 

    page = parseInt(page as any)
    if(page === undefined || page < 1) page = 1
    
    options.limit = limit;
    options.skip = (page-1) * limit;

    const data = await this.articleService.find(filter, options);
    const total = await this.articleService.count(filter);

    return {
      total,
      pages: Math.ceil(total / limit),
      currentPage: page,
      itemsPage: data.length,
      data,
    };
    
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: string, @Res() res: Response) {
    this.logger.log(`Removing article with id ${id}`);
    const result = await this.articleService.delete(id)
    if(result.deletedCount > 0){
      res.status( HttpStatus.NO_CONTENT).send()
    } else {
      res.status(HttpStatus.NOT_FOUND).send({message: "Error in article delete"})

    }
  }


  @Cron('0 0 * * * *') //execute once every 1 hour at min 0 sec 0
  async refreshAlgoliaArticles(page: number = 0){

    const PERIOD_TIME = 60 * 60 * 1 // one hour
    const date = moment();
    const algoliaQuery = process.env.ALGOLIA_QUERY;
    const msDate = date.unix() - PERIOD_TIME;

    this.logger.log(`Retriving algolia articles at ${date.format()} from ${algoliaQuery}, page ${page}, msDate ${msDate}`);
    this.logger.log(``);

    this.algoliaService.fetchRecords(algoliaQuery, msDate, page).subscribe(
      async ({data}) => {
        this.logger.log(`Total Search Hits ${data.nbHits} and pages ${data.nbPages}`);
        await this.articleService.insertMany(data.hits);
        if((page+1) < data.nbPages){
          this.refreshAlgoliaArticles(page+1);
        }
      }, 
      (err) => this.logger.error(`Has ocurred an error fetching algolia records ` + err),
      () => this.logger.log(`Algolia refresh query finished page ${page}`)
    );  
  }

  parseMonth(searchText: string): number{
    switch(searchText.toLocaleLowerCase()){
      case 'january':
        return 1;
      case 'february':
        return 2;
      case 'march':
        return 3;
      case 'april':
        return 4;
      case 'may':
        return 5;
      case 'june':
        return 6;
      case 'july':
        return 7;
      case 'august':
        return 8;
      case 'september':
        return 9;
      case 'october':
        return 10;
      case 'november':
        return 11;
      case 'december':
        return 12;  
      default:
        return null
    }
  }
}