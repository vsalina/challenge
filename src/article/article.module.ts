import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';
import { ArticleController } from './article.controller';
import { Article, ArticleSchema } from './article.entity';
import { ArticleService } from './article.service';
import { AlgoliaService } from './algolia/algolia.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Article.name, schema: ArticleSchema }]),
    HttpModule.register({
      timeout: 30000,
      maxRedirects: 5,
    }),
  ],
  controllers: [ArticleController],
  providers: [ArticleService, AlgoliaService,]
})
export class ArticleModule {}
