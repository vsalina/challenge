import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { AlgoliaService } from './algolia.service';

describe('AlgoliaService', () => {
  let service: AlgoliaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule.register({
        timeout: 30000,
        maxRedirects: 5,
      }),],
      providers: [AlgoliaService],
    }).compile();

    service = module.get<AlgoliaService>(AlgoliaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return Algolia Data', (done) => {
      service.fetchRecords("http://hn.algolia.com/api/v1/search_by_date?query=nodejs",1632508127, 0).subscribe((data)=>{
        expect(data.data.hits.length).toBeGreaterThan(0)
        expect(data.data.nbPages).toBeGreaterThan(0)
        done()
      }, err => fail(err))
  }, 30000)
});
