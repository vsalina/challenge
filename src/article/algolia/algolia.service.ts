import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { Article } from '../article.entity';



@Injectable()
export class AlgoliaService {
  constructor(private httpService: HttpService) {}

  fetchRecords(algoliaQuery, msDate, page): Observable<AxiosResponse<ServerResponse>> {
    return this.httpService.get(`${algoliaQuery}&numericFilters=created_at_i>${msDate}&page=${page}`)
  }
}

interface ServerResponse {
  hits: Article[]
  nbHits: number,
  page: number,
  nbPages: number,
  hitsPerPage: number,
  exhaustiveNbHits: number,
  query: string,
  params: string,
  renderingContent: any,
  processingTimeMS: number
}
