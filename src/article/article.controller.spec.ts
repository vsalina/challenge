require("dotenv").config();
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { doesNotMatch } from 'assert';
import { AlgoliaService } from './algolia/algolia.service';
import { ArticleController } from './article.controller';
import { Article, ArticleSchema } from './article.entity';
import { ArticleService } from './article.service';

describe('ArticleController', () => {
  let controller: ArticleController;
  function getMongoUrl(){
    const url = process.env.MONGO_URL || 'localhost';
    const user = process.env.MONGO_USERNAME || 'user';
    const password = process.env.MONGO_PASSWORD || '123456';
  
    return `mongodb://${user}:${password}@${url}/challenge?authSource=admin&readPreference=primary&ssl=false`
  }
  
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(getMongoUrl(), {
          autoCreate: true
        }),
        MongooseModule.forFeature([{ name: Article.name, schema: ArticleSchema }]),
        HttpModule.register({
          timeout: 30000,
          maxRedirects: 5,
        }),
      ],
      controllers: [ArticleController],
      providers: [ArticleService, AlgoliaService,]
    }).compile();

    controller = module.get<ArticleController>(ArticleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('Parse month', ()=> {
    expect(controller.parseMonth("january")).toEqual(1)
    expect(controller.parseMonth("february")).toEqual(2)
    expect(controller.parseMonth("march")).toEqual(3)
    expect(controller.parseMonth("april")).toEqual(4)
    expect(controller.parseMonth("May")).toEqual(5)
    expect(controller.parseMonth("june")).toEqual(6)
    expect(controller.parseMonth("July")).toEqual(7)
    expect(controller.parseMonth("august")).toEqual(8)
    expect(controller.parseMonth("September")).toEqual(9)
    expect(controller.parseMonth("October")).toEqual(10)
    expect(controller.parseMonth("November")).toEqual(11)
    expect(controller.parseMonth("december")).toEqual(12)

  })
});
