require("dotenv").config();
import {seeder} from "nestjs-seeder";
import {ArticleSeeder} from "./article.seeder";
import {MongooseModule} from "@nestjs/mongoose";
import {Article, ArticleSchema} from "./article.entity";

function getMongoUrl(){
    const url = process.env.MONGO_URL || 'localhost';
    const user = process.env.MONGO_USERNAME || 'user';
    const password = process.env.MONGO_PASSWORD || '123456';
  
    return `mongodb://${user}:${password}@${url}/challenge?authSource=admin&readPreference=primary&ssl=false`
  }
seeder({
    imports: [
        MongooseModule.forRoot(getMongoUrl()),
        MongooseModule.forFeature([{name: Article.name, schema: ArticleSchema}])
    ]
}).run([ArticleSeeder]);