import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import { Factory } from "nestjs-seeder";

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Factory(faker => faker.date.recent(100))
  @Prop()
  created_at: Date;

  @Factory(faker => faker.lorem.words(10))
  @Prop()
  title: string;

  @Factory(faker => faker.internet.url())
  @Prop()
  url: string;
  
  @Factory(faker => faker.internet.userName())
  @Prop()
  author: string;


  @Prop()
  points?: string;

  @Prop()
  story_text?: string;

  @Factory(faker => faker.lorem.words(25))
  @Prop()
  comment_text: string;
  @Factory(faker => faker.random.number(100))
  @Prop()
  num_comments: number;

  @Factory(faker => faker.random.number(99999999))
  @Prop()
  story_id: number;

  @Factory(faker => faker.lorem.words(7))
  @Prop()
  story_title: string;

  @Factory(faker => faker.internet.url())
  @Prop()
  story_url: string;

  @Factory(faker => faker.random.number(99999999))
  @Prop()
  parent_id: number;

  @Factory(faker => faker.random.arrayElement([
    'node',
    'api',
    'rest'
  ]))
  @Prop([String])
  _tags: string[];
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
