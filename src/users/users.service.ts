import { Injectable } from '@nestjs/common';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
  private readonly users = [
    {
      userId: 1,
      username: 'jose',
      password: '123456', // I know this has to be encrypted
    },
    {
      userId: 2,
      username: 'guess',
      password: 'guess', // I know this has to be encrypted
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user => user.username === username);
  }
}