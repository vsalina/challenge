import { Controller, Post, UseGuards, Res, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './auth.guard';
import { Response, Request} from 'express';

@Controller('api/v1')
export class AuthController {

  constructor(private authService: AuthService) {}


  @Post('auth/login')
  async login(@Req() req: Request, @Res() res: Response) {
    const result = await this.authService.login(req.body);
    if(!result){
      return res.status(400).send({
        Error: "Invalid Credentials"
      })
    }
    return res.status(200).send(result)
  }
}