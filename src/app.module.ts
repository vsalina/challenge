require("dotenv").config();
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleModule } from './article/article.module';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';


function getMongoUrl(){
  const url = process.env.MONGO_URL || '127.0.0.1:27017';
  const user = process.env.MONGO_USERNAME || 'user';
  const password = process.env.MONGO_PASSWORD || '123456';
  return `mongodb://${user}:${password}@${url}/challenge?authSource=admin&readPreference=primary&ssl=false`
}

@Module({
  imports: [  
    ConfigModule.forRoot(),
    MongooseModule.forRoot(getMongoUrl(), {
      autoCreate: true
    }),
    ScheduleModule.forRoot(),
    ArticleModule,
    AuthModule,
    UsersModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
