require("dotenv").config();
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
  .setTitle('Algolia Articles Challence')
  .setDescription("The server, once an hour, connect to Algolia API to look for posted articles about Node.js on Hacker News. It should insert the data from the API into MongoDB and expose them by REST API")
  .setVersion('1.0')
  .build();
const document = SwaggerModule.createDocument(app, config);
SwaggerModule.setup('api/docs', app, document);

  await app.listen(3000);
}
bootstrap();
