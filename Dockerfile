# Docker Multistage construction 
FROM node:14.17.3 AS build

#  Navigate to the container working directory 
WORKDIR /usr/src/app
#  Copy package.json
COPY package*.json ./

RUN npm install glob rimraf
RUN npm install
COPY . .
RUN npm run build


FROM build AS test
RUN npm run test


FROM build as production
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
RUN npm ci --production
COPY . .
EXPOSE 3000
CMD ["node", "dist/main.js"]